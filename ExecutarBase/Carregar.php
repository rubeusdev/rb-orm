<?php
namespace Rubeus\ORM\ExecutarBase;
use Rubeus\ORM\GerenciarObjeto\GerenciarObjeto as GerenciarObjeto;
use Rubeus\Bd\Persistencia as Persistencia;
use Rubeus\Query\Query as Query;

class Carregar{
    private $tabela;
    private $xml;
    private $query;
    
    private function where(){
        $filtro = $this->tabela->getFiltro();
        if($filtro == ''){
            $filtro = " where ".GerenciarObjeto::campoAtributo($this->tabela, $this->xml, ' and ',true); 
        }
        if(trim($filtro) == 'where') $filtro = "";
        return $filtro;
    }
    
    private function order(){
        return $this->tabela->getOrder();
    }
    
    private function limit(){
        return $this->tabela->getLimit();
    }
    
    private function filtro(){
        return $this->where().' '.$this->order().' '.$this->limit();
    }
    
    public function carregar($tabela, $xml, $condicionais,$campos='*',$obj=true,$filtroResultado='AA',$indice=''){
        $this->tabela = $tabela;
        $this->xml = $xml;
        $this->query = $condicionais;
        
        $dados = Persistencia::consultar(false, "select $campos from `".$this->xml->getTabela()."` ".$this->filtro()." ",false);
        
        if($obj) return $this->popular($dados);
        
        $query = new Query();
        $query->filtrar($filtroResultado, $dados, $indice);
        return $dados;
    }
    
    private function popular($dados){
        if(!is_array($dados) || !isset($dados[0])){
            $this->tabela->set('id', false); 
            return false;
       }
       if(count($dados) > 1){
           $this->tabela->data = $dados;
           $this->tabela->tmData = count($dados);
       }
       return GerenciarObjeto::popularObjeto($this->tabela,$dados[0]);
    }
    
}

