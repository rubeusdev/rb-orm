<?php
namespace Rubeus\ORM\ExecutarBase;
use Rubeus\ORM\GerenciarObjeto\GerenciarObjeto as GerenciarObjeto;
use Rubeus\Bd\Persistencia as Persistencia;

class Salvar{
    private $tabela;
    private $query;
    private $xml;
    private $id;
    private $string;
    private $sql;

    private function percorrerData($i,&$valores,&$indice,$update,$inserir=true){
        if(!is_null($i)){
            $this->tabela->percorrer($i);
        }

        $existeId = $this->tabela->get('id');
        if($existeId == 0 ){
            $this->tabela->set('id',false);
        }
        if(($existeId && $existeId) || $update) {
            $id = $this->update($existeId);
        }else{
            if(count($valores) == 0){
                $indice = $i;
            }
            $valores[] = GerenciarObjeto::atributos($this->tabela,',');
            if($inserir){
                $id = $this->insert(implode(',',$valores));
                if(count($valores) > 0 && $id){
                    for($i=0;$i<count($valores);$i++){
                        $this->id[] = $id + $i;
                        if(count($valores)>1){
                            $this->tabela->data[$i]['id'] = $id + $i;
                        }
                    }
                    $valores = [];
                }
                $this->tabela->set('id',end($this->id));
            }
        }
    }

    public function salvar($tabela,$xml,$condicao,$update=false,$string=false, $unicoInsert=false){
        $this->string = $string;
        $this->tabela = $tabela;
        $this->xml = $xml;
        $this->query = $condicao;

        $this->id = array();
        $valores = array();
        $indice = 0;

        $this->tabela->tmData = 0;
        $this->tabela->montarData();
        if($this->tabela->getQtdData()>0){
            for($i=0;$i<$this->tabela->getQtdData();$i++){
                $this->percorrerData( $i, $valores,$indice, $update, !$unicoInsert || $this->tabela->getQtdData() == $i+1);
            }
            $this->tabela->percorrer();
        }else{
            $this->percorrerData( null, $valores,$indice, $update);
        }

        /*if(count($this->id) > 1){
            $this->tabela->set('id',$this->id);
        }else{
            $this->tabela->set('id',$this->id[0]);
        }*/

        if($this->string){
            return $this->sql;
        }
        if($this->tabela->getQtdErro()){
            return false;
        }
        return true;
    }

     private function where($id){
        $filtro = '';
        if(!is_null($this->query)){
            $filtro = $this->query->where()->string();
        }
        if($filtro == '' && !is_array($id)){
            $filtro = " where id = ".$id;
        }elseif($filtro == '' && is_array($id)){
            $filtro = " where id in (" . implode(", ", $id) . ")";
        }
        return $filtro;
    }

    public function update($idRegistro){

        $sql = "update `".$this->xml->getTabela()."` set ".GerenciarObjeto::campoAtributo($this->tabela,$this->xml,',').' '.$this->where($idRegistro);

        if(!$this->string){
            $id = Persistencia::execultar($sql);
            if($id === false){
                $this->tabela->setErro(array('status' => 'falhou','sentenca' => $sql));
            }
        }else{
            $this->sql = $sql;
        }
        $this->id[] = ($id>0)?$id:$idRegistro;
        return $id;
    }

    private function insert($valores,$todos=false){
        $this->tabela->setId(false);
        $campos = GerenciarObjeto::campos($this->tabela,',',$todos);

        $sql = "insert into `" .$this->xml->getTabela(). "`(".$campos.") values $valores ;";
        if(!$this->string){
            $id = Persistencia::execultar($sql);
            if($id === false){
                $this->tabela->setErro(array('status' => 'falhou','sentenca' => $sql));
            }
        }else{
            $this->sql = $sql;
        }
        //$this->id[] = $id;
        return $id;
    }

}
