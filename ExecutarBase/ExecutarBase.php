<?php
namespace Rubeus\ORM\ExecutarBase;

abstract class ExecutarBase{
    static private $salvar;
    static private $carregar;
    static private $deletar;

    public static function salvar($tabela,$xml, $condicionais,$update=false,$string=false,$unicoInsert=false){
        if(is_null(self::$salvar))
            self::$salvar = new Salvar();
        return self::$salvar->salvar($tabela,$xml, $condicionais,$update, $string,$unicoInsert);
    }

    public static function carregar($tabela,$xml, $condicionais,$campos='*',$obj=true,$filtroResultado='AA',$indice=false){
        if(is_null(self::$carregar))
            self::$carregar = new Carregar();
        return self::$carregar->carregar($tabela,$xml,  $condicionais,$campos,$obj,$filtroResultado,$indice);
    }

    public static function deletar($tabela,$xml,  $condicionais,$tipo=0){
        if(is_null(self::$deletar))
            self::$deletar = new Delete();
        return self::$deletar->deletar($tabela,$condicionais, $xml,$tipo);
    }

}
