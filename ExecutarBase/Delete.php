<?php
namespace Rubeus\ORM\ExecutarBase;
use Rubeus\ORM\GerenciarObjeto\GerenciarObjeto as GerenciarObjeto;
use Rubeus\Bd\Persistencia as Persistencia;

class Delete{
    private $tabela;
    private $sql;
    private $query;
    private $xml;
    
    private function where(){
        $filtro = '';
        if(!is_null($this->query))
            $filtro = $this->query->where()->string();
        if($filtro == '')
            $filtro = " where ".GerenciarObjeto::campoAtributo($this->tabela, $this->xml,' and ',true); 
        if(trim($filtro) == 'where') $filtro = "";
        return $filtro;
    }
    
    private function desativar(){
        $this->sql = "update ".$this->xml->getTabela()." set ativo = 0 ".$this->where();
        return Persistencia::execultar($this->sql); 
    }
    
    private function delete(){
        $this->sql = "delete from ".$this->xml->getTabela()." ".$this->where();
        return Persistencia::execultar($this->sql); 
    }
    
    public function deletar($tabela,$condicional, $xml,$tipo=0){
        $this->xml = $xml;
        $this->query = $condicional;
        $this->tabela = $tabela;
        
        if($tipo == 1)
            $resultado = $this->delete();
        else $resultado = $this->desativar();
        
        if($resultado === false) $tabela->setErro(array('status' => 'falhou','sentenca' => $this->sql));
        
        return $resultado;
    }
    
}