<?php
namespace Rubeus\ORM;
use Rubeus\ORM\GerenciarObjeto\GerenciarObjeto as GerenciarObjeto;
use Rubeus\ORM\ExecutarBase\ExecutarBase as ExecutarBase;

abstract class Persistir{
    private static $xml;
    private static $data;
    private static $tmData;
    private static $query;
    private static $erro;
    private static $persistirErro=false;
    private static $diretorioMapeamento;
    private static $classeMapeamento;
    private static $entidade;
    
    public static function setEntidade($entidade, $diretorio=false, $classe=false){
        $this->entidade = $entidade;
        if($diretorio){
            $this->diretorioMapeamento = $diretorio;
            $this->classeMapeamento = $classe;
        }else{
            $classe = new Classe($entidade);
            $this->diretorioMapeamento = $classe->getDiretorio();
            $this->classeMapeamento = $classe->getNome();
        }
        $this->iniciarXml(true);
    }
    
    public  static function getXml() {
        return $this->xml;
    }

    public  static  function iniciarXml($iniciar=false){  
        if(is_null($this->xml) || $iniciar)
            $this->xml = new MapXML\MapXML($this->diretorioMapeamento, $this->classeMapeamento);          
    }
    
    public static function getPersistirErro() {
        return $this->persistirErro;
    }

    public static function setPersistirErro($persistirErro) {
        $this->persistirErro = $persistirErro;
    }
     
    public  static  function getErro() {
        return $this->erro;
    }
    
    public  static function getQtdErro() {
        return count($this->erro);
    }
    
    public  static function setErro($erro) {
        $this->erro[] = $erro;
    }

    public  static function limparErro($persistir=false){
        if(!$this->persistirErro || $persistir)
            $this->erro = Array();
    }
    
    public  static  function verificarQuery(){
        if(is_null($this->query))$this->query = new Query();        
    }
    
    public  static  function getFiltro(){
        if(is_null($this->query))return '';
        return  $this->tabela->query->where()->string();
    }
    
    public  static  function getOrder(){
        if(is_null($this->query))return '';
        return $this->tabela->query->order()->string();
    }
    
    public static  function getLimit(){
        if(is_null($this->query))return '';
        return $this->tabela->query->limit()->string();
    }
    
    public  static  function filtro($condicao=false,$ligacao=false){
        $this->verificarQuery();
        return $this->query->where($condicao,$ligacao);
    }
    
    public static  function order($order=false,$sentido=false){
        $this->verificarQuery();
        return $this->query->order($order,$sentido);
    }
    
    public static  function limit($inicio=false,$fim=false){
        $this->verificarQuery();
        return $this->query->limit($inicio,$fim);
    }
    
    public  static function getClausula(){
        return new Query();
    }
      
    public static  function getData(){
        return $this->data;
    }
    
    public  static function getQtdData(){
        return $this->tmData;
    }
    
    public  static function addData($indice=false){
        if(!$indice)$indice = $this->entidade->getQtdData();
        GerenciarObjeto::popularDados($this->entidade,$this->entidade->getData(),$indice);
        $this->limparObjeto(false);
    }
    
    public  static function limparObjeto($limparData=true){
        GerenciarObjeto::limparObjeto($this->entidade,$limparData);
    }
    
    public static  function limparFiltro(){
        $this->query = null;
    }
    
    public  static function limparData(){
        $this->data = array();
        $this->tmData = 0;
    }
         
    public  static function percorrer($indice){
        GerenciarObjeto::popularObjeto($this->entidade,$this->data[$indice]);
    }
    
    public  static function carregar($campos='*',$obj=true,$filtro='AA'){
        return ExecutarBase::carregar($this->entidade, $campos, $obj,$filtro);
    } 
    
    public  static function salvar($commit=false, $update=false){
        $this->limparErro(true);
        return ExecutarBase::salvar($this->entidade, $commit, $update);
    }
    
    public  static function commit(){
        $this->limparErro(true);
        return Persistencia::commit();
    }
    
    public  static function roolBack(){
        $this->limparErro(true);
        return Persistencia::roolBack();
    }
    
    public  static function deletar($tipo=0, $commit = false){
        $this->limparErro(true);
        return ExecutarBase::deletar($this->entidade,$tipo, $commit);
    }
    
}
