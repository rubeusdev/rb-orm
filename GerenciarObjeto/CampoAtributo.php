<?php
namespace Rubeus\ORM\GerenciarObjeto;
class CampoAtributo{
    private $tabela;
    private $campos;
    private $xml;
    
    private function stringCampoAtributo($atributo,$coluna,$null){        
        if(is_null($atributo) || (trim($atributo) == '' && !is_array($atributo))){
            return $coluna.$null;
        }
        if(intval($atributo) == $atributo){
            return  $coluna." = '".addslashes($atributo)."'"; 
        }
        if(is_array($atributo)){
            return $coluna." in ('".implode("','",$atributo)."') ";  
        }
        if($atributo instanceof Query){
            return $coluna." = (".$atributo->string().") ";
        }
        return $coluna." = '".addslashes($atributo)."' ";  
    }
    
    private  function percorrer($null){   
        $atributo = $this->tabela->get($this->xml->getAtributo(),true);
        if($atributo !== false){
            $this->campos[] = $this->stringCampoAtributo($atributo, $this->xml->getColuna(), $null);
        }
        if($this->xml->proximo()){
            $this->percorrer($null);
        }
    }
    
    public function camposAtributos($tabela, $xml,$conector,$consulta=false){
        $this->tabela = $tabela;
        $this->xml = $xml;
        $this->campos = array();
        $null = $consulta ? ' is null ':' = null ';
        
        $this->percorrer($null);
       
        return implode($conector, $this->campos);
    }
    
    private function stringAtributo($atributo){       
        if(is_null($atributo)  || trim($atributo) == ''){
            return "null"; 
        }
        if(intval($atributo) == $atributo){
            return "'".addslashes($atributo)."'"; 
        }
        if($atributo instanceof Query){
            return " (".$atributo->string().") ";
        }
        if(is_object($atributo)){
            return $this->stringAtributo($atributo->getId());  
        }
        return " '".addslashes($atributo)."' ";  
    }
    
    private function percorrerAtributos(){
        $atributo = $this->tabela->get($this->xml->getAtributo(),true);
        
        if($atributo !== false){
            $this->campos[] = $this->stringAtributo($atributo);
        }
        if($this->xml->proximo()){
            $this->percorrerAtributos();
        }
    }
    
    public function atributos($tabela,$conector){
        $this->tabela = $tabela;
        $this->xml = $this->tabela->getXML();
        $this->campos = array();
        $this->percorrerAtributos();
        return '('.implode($conector, $this->campos).')';
    }
    
    private function percorrerCampos($tudo){
        $atributo = $this->tabela->get($this->xml->getAtributo(),true);
        if($atributo !== false || $tudo){
            $this->campos[] = $this->xml->getColuna();
        }
        if($this->xml->proximo()){
            $this->percorrerCampos($tudo);
        }
    }
    
    public function campos($tabela,$conector,$tudo=false){
        $this->tabela = $tabela;
        $this->xml = $this->tabela->getXML();
        $this->campos = array();
        $this->percorrerCampos($tudo);
        return implode($conector, $this->campos);
    }  
    
}