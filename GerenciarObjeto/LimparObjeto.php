<?php
namespace Rubeus\ORM\GerenciarObjeto;

class limparObjeto{
    
    public function limparObjeto($tabela,$limparData=true){
        do{
            $tabela->set($tabela->getXml()->getAtributo(),false); 
        }while($tabela->getXml()->proximo());
       
        $tabela->set('id',false);        
        if($limparData)$tabela->limparData();
        $tabela->limparFiltro();
    }

}