<?php
namespace Rubeus\ORM\GerenciarObjeto;

abstract class GerenciarObjeto{
    static private $popularObjeto;
    static private $limparObjeto;
    static private $popularDados;
    static private $campoAtributo;

    public static function popularObjeto($tabela,$resultado){
        if(is_null(self::$popularObjeto))
            self::$popularObjeto = new PopularObjeto();
        return self::$popularObjeto->popularObjeto($tabela,$resultado);
    }

    public static function limparObjeto($tabela,$limparData=true){
        if(is_null(self::$limparObjeto))
            self::$limparObjeto = new LimparObjeto();
        return self::$limparObjeto->limparObjeto($tabela,$limparData);
    }

    public static function popularDados($tabela,&$data,&$indice,$incrementar=true){
        if(is_null(self::$popularDados))
            self::$popularDados = new PopularDados();
        return self::$popularDados->popularData($tabela,$data,$indice,$incrementar);
    }

    public static function campoAtributo($tabela, $xml, $conector,$consulta=false){
        if(is_null(self::$campoAtributo))
            self::$campoAtributo = new CampoAtributo();
        return self::$campoAtributo->camposAtributos($tabela, $xml, $conector,$consulta);
    }

    public static function atributos($tabela,$conector){
        if(is_null(self::$campoAtributo))
            self::$campoAtributo = new CampoAtributo();
        return self::$campoAtributo->atributos($tabela,$conector);
    }

    public static function campos($tabela,$conector,$tudo=false){
        if(is_null(self::$campoAtributo))
            self::$campoAtributo = new CampoAtributo();
        return self::$campoAtributo->campos($tabela,$conector,$tudo);
    }
}
