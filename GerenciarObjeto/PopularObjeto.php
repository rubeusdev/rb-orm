<?php
namespace Rubeus\ORM\GerenciarObjeto;

class PopularObjeto{

    public function popularObjeto($tabela,$resultado){
        if(!is_array($resultado)){
            $tabela->set('id', false);
            return false;
        }
        $arrayKeys = array_keys($resultado);
        do{
            if(in_array($tabela->getXml()->getColuna(), $arrayKeys)){
                $tabela->set($tabela->getXml()->getAtributo(),$resultado[$tabela->getXml()->getColuna()]);
            }else{
                $tabela->set($tabela->getXml()->getAtributo(),false);
            }
        }while($tabela->getXml()->proximo());

        //$tabela->set('id', $resultado['id']);
        return true;
    }
}
