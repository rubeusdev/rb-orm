<?php
namespace Rubeus\ORM\GerenciarObjeto;

class PopularDados{

    public function popularData($tabela,&$data,&$indice,$incrementar=true){
        do{
            $valorAtributo = $tabela->get($tabela->getXml()->getAtributo());
            if(is_object($valorAtributo)){
                $atributo = $data[$indice][$tabela->getXml()->getColuna()] = $tabela->get($tabela->getXml()->getAtributo())->getId();
            }else{
                $atributo = $data[$indice][$tabela->getXml()->getColuna()] = $tabela->get($tabela->getXml()->getAtributo());
            }
            if($atributo && $incrementar) unset($data[$indice][$tabela->getXml()->getColuna()]);
        }while($tabela->getXml()->proximo());

        if($tabela->getId()) $data[$indice]['id'] = $tabela->getId();

        if(count($data[$indice]) > 0 && is_array($data[$indice]) && $incrementar)$indice++;
    }

}
