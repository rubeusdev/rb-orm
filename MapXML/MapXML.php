<?php
namespace Rubeus\ORM\MapXML;
use Rubeus\Bd\Persistencia as Persistencia;

class MapXML{
    private $coluna;
    private $atributo;
    private $tabela;
    private $entidade;
    private $posicao;
    private $indice;
    private $outrasPropriedades;

    public function __construct($diretorio, $classe) {
        $xml = Persistencia::lerXML($diretorio,$classe);
        $this->tabela = rtrim($xml['tabela']);
        $this->entidade = rtrim($xml['nome']);
        $this->set('id','id');
        $this->percorrer($xml->nparaum);
        $this->percorrer($xml->propriedades);
        $this->pecorrerOutrosAtributos($xml);
        $this->indice = 0;
    }

    private function  pecorrerOutrosAtributos($xml){

        $this->outrasPropriedades = array();

        foreach($xml->propriedades as $propriedade){
            foreach($propriedade->attributes() as $chave=>$valor){
                if(rtrim($chave) !== 'atributo' && rtrim($chave) != 'coluna'){
                    $this->outrasPropriedades[rtrim($chave).'::'.rtrim($valor)] = array(
                        'coluna' => rtrim($propriedade['coluna']),
                        'atributo' => rtrim($propriedade['atributo'])
                    );
                }
            }

        }

        foreach($xml->nparaum as $propriedade){
            foreach($propriedade as $chave=>$valor){

                if(rtrim($chave) !== 'atributo' && rtrim($chave) != 'coluna'){
                    $this->outrasPropriedades[rtrim($chave).'::'.rtrim($valor)] = array(
                        'coluna' => rtrim($propriedade['coluna']),
                        'atributo' => rtrim($propriedade['atributo'])
                    );
                }
            }

        }
    }

    private function  percorrer($array){
        $qtd = count($array);
        for($i=0; $i<$qtd; $i++)
            $this->set(rtrim($array[$i]['coluna']), rtrim($array[$i]['atributo']));
    }

    private function set($coluna, $atributo){
       $this->coluna[$atributo] = $coluna;
       $this->atributo[$coluna] = $atributo;
       $this->posicao[] = $atributo;
    }

    public function getColuna($chave=false){
        if(!$chave)$chave = $this->posicao[$this->indice];
        return $this->coluna[$chave];
    }

    public function getPropriedade($chave,$indice="atributo"){
        return $this->outrasPropriedades[$chave][$indice];
    }

    public function getAtributo($chave=false){
        if(!$chave) return $this->posicao[$this->indice];
        return $this->atributo[$chave];
    }

    public function proximo(){
        $this->indice++;
        if($this->indice == count($this->coluna)){
            $this->indice = 0;
            return false;
        }
        return true;
    }

    public function getQtdPropriedade(){
        return count($this->xml->propriedades);
    }

    public function getTabela(){
        return $this->tabela;
    }

    public function getEntidade(){
        return $this->entidade;
    }

    public function getAtributos(){
        return $this->posicao;
    }
}
