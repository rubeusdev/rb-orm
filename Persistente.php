<?php

namespace Rubeus\ORM;

use Rubeus\ORM\GerenciarObjeto\GerenciarObjeto as GerenciarObjeto;
use Rubeus\ORM\ExecutarBase\ExecutarBase as ExecutarBase;
use Rubeus\Servicos\Classe\Classe as Classe;
use Rubeus\Query\Query as Query;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\ORM\MapXML\MapXML;

class Persistente
{
    private $xml;
    public $data;
    public $tmData;
    private $query;
    private $erro;
    private $persistirErro;
    private $diretorioMapeamento;
    private $classeMapeamento;
    private $indiceAtual;
    private $unicoInsert;

    public function __construct($id = false)
    {
        if ($id) $this->__set('id', $id);
        $this->persistirErro = false;
        $this->limparData();
        $this->limparErro();
        $classe = new Classe($this);
        $this->diretorioMapeamento = $classe->getDiretorio();
        $this->classeMapeamento = $classe->getNome();
        $this->nomeClasse = $classe->getClasse();
        $this->iniciarXml();
        $this->indiceAtual = 0;
    }

    public function getClasse()
    {
        return $this->nomeClasse;
    }

    public function getEntidade()
    {
        return $this->xml->getEntidade();
    }

    public function getIds($valorPadrao = false)
    {
        $id = array();
        if ($this->getId() && !is_null($this->getId())) {
            $id[] = $this->getId();
        }
        $this->percorrer(1);
        if ($this->getId() && !is_null($this->getId()) && $this->getId() != $id[0]) {
            $id[] = $this->getId();
            while ($this->percorrer()) {
                $id[] = $this->getId();
            }
        }
        if (empty($id)) return $valorPadrao;
        return $id;
    }

    public function getCampos()
    {
        return $this->xml->getAtributos();
    }

    public function getXml()
    {
        return $this->xml;
    }

    public function getClasseMap()
    {
        return $this->classeMapeamento;
    }

    public function iniciarXml()
    {
        if (is_null($this->xml))
            $this->xml = new MapXML($this->diretorioMapeamento, $this->classeMapeamento);
    }

    public function getPersistirErro()
    {
        return $this->persistirErro;
    }

    public function setPersistirErro($persistirErro)
    {
        $this->persistirErro = $persistirErro;
    }

    public function getErro()
    {
        return $this->erro;
    }

    public function getQtdErro()
    {
        return count($this->erro);
    }

    public function setErro($erro)
    {
        $this->erro[] = $erro;
    }

    public function limparErro($persistir = false)
    {
        if (!$this->persistirErro || $persistir)
            $this->erro = array();
    }

    public function verificarQuery()
    {
        if (is_null($this->query)) $this->query = new Query();
    }

    public function getFiltro()
    {
        if (is_null($this->query)) return '';
        return  $this->query->where()->string();
    }

    public function getOrder()
    {
        if (is_null($this->query)) return '';
        return $this->query->order()->string();
    }

    public function getLimit()
    {
        if (is_null($this->query)) return '';
        return $this->query->limit()->string();
    }

    public function filtro($condicao = false, $ligacao = false)
    {
        $this->verificarQuery();
        return $this->query->where($condicao, $ligacao);
    }

    public function order($order = false, $sentido = false)
    {
        $this->verificarQuery();
        return $this->query->order($order, $sentido);
    }

    public function limit($inicio = false, $fim = false)
    {
        $this->verificarQuery();
        return $this->query->limit($inicio, $fim);
    }

    public function getClausula()
    {
        return new Query();
    }

    public function getData($indice = false)
    {
        if ($indice) {
            $array = [];
            for ($i = 0; $i < count($this->data); $i++) {
                if (isset($this->data[$i][$indice])) {
                    $array[]  = $this->data[$i][$indice];
                } else if (isset($this->data[$i][$this->xml->getColuna($indice)])) {
                    $array[]  = $this->data[$i][$this->xml->getColuna($indice)];
                }
            }
            return $array;
        }
        return $this->data;
    }

    public function getQtdData()
    {
        if ($this->tmData == 0 && count($this->data) > 0) {
            $this->tmData = count($this->data);
        }
        return $this->tmData;
    }

    public function addData($indice = false)
    {
        if (!$indice) {
            $indice = $this->tmData;
        }
        GerenciarObjeto::popularDados($this, $this->data, $indice);
        $this->limparObjeto(false);
    }

    public function limparObjeto($limparData = true)
    {
        GerenciarObjeto::limparObjeto($this, $limparData);
    }

    public function limparFiltro()
    {
        $this->query = null;
    }

    public function limparData()
    {
        $this->data = array();
        $this->tmData = 0;
    }

    public function percorrer($indice = false)
    {
        GerenciarObjeto::popularDados($this, $this->data, $this->indiceAtual, false);
        if ($indice === false) {
            $indice = ++$this->indiceAtual;
        }
        if ($indice >= $this->tmData) {
            $this->indiceAtual = $indice = 0;
        }
        if (isset($this->data[$indice])) {
            GerenciarObjeto::popularObjeto($this, $this->data[$indice]);
            $this->indiceAtual = $indice;
        }
        return $indice;
    }

    public function carregar($campos = '*', $obj = true, $filtro = 'AA', $indice = false)
    {
        return ExecutarBase::carregar($this, $this->xml, $this->query, $campos, $obj, $filtro, $indice);
    }

    public function setUnicoInsert($unicoInsert)
    {
        $this->unicoInsert = $unicoInsert;
    }

    public function salvar($update = false, $string = false, $commit = false)
    {
        $this->limparErro(true);
        //        $registrar = Conteiner::get('RegistrarObjetos');
        //        if($registrar){
        //            $objRegistrar = Conteiner::get($registrar);
        //            $objRegistrar->addObjeto(clone $this);
        //        }

        return ExecutarBase::salvar($this, $this->xml, $this->query, $update, $string, $this->unicoInsert);
    }

    public function commit()
    {
        $this->limparErro(true);
        return Persistencia::commit();
    }

    public function roolBack()
    {
        $this->limparErro(true);
        return Persistencia::roolBack();
    }

    /**
     * @param integer $tipo `0` para softdelete e `1` para harddelete
     */
    public function deletar($tipo = 0, $commit = false)
    {
        $this->limparErro(true);
        //        $registrar = Conteiner::get('RegistrarObjetos');
        //        if($registrar){
        //            $objRegistrar = Conteiner::get($registrar);
        //            $clone = clone $this;
        //            $clone->setAtivo(0);
        //            $objRegistrar->addObjeto($clone);
        //        }
        return ExecutarBase::deletar($this, $this->xml, $this->query, $tipo, $commit);
    }

    public function get($key, $valor = false)
    {
        $operacao = "get" . ucfirst($key);
        $metodos = get_class_methods($this);
        if (in_array($operacao, $metodos)) {
            $propriedade = $this->$operacao();
            if ($valor && is_object($propriedade)) return $propriedade->getId();
            return $propriedade;
        }
    }

    public function setAttrXml($attr, $valorAttr, $valor)
    {
        $this->set($this->xml->getPropriedade($attr . '::' . $valorAttr, 'atributo'), $valor);
    }

    public function getAttrXml($attr, $valorAttr)
    {
        return $this->get($this->xml->getPropriedade($attr . '::' . $valorAttr, 'atributo'));
    }

    public function setColumn($key, $valor)
    {
        if (!is_null($this->xml->getAtributo($key))) {
            $this->set($this->xml->getAtributo($key), $valor);
        }
    }

    public function set($key, $valor)
    {
        $operacao = "set" . ucfirst($key);
        $metodos = get_class_methods($this);

        if (in_array($operacao, $metodos)) {
            if (is_array($valor) && count($valor) > 0) {
                $this->adicionarData($key, $valor);
            } else if (!is_array($valor)) {
                $this->$operacao($valor);
            }
        }
    }

    private function adicionarData($key, $array)
    {
        $qtd = count($array);
        for ($i = 0; $i < $qtd; $i++) {
            if (!is_array($array[$i]) && $array[$i] !== false) {
                $this->data[$i][$this->xml->getColuna($key)] = $array[$i];
            }
        }
        $this->tmData = $qtd;
        $this->set($key, $array[0]);
    }

    public function montarData()
    {
        $propriedade = $this->get($this->xml->getAtributo());
        if (is_object($propriedade)) {
            $ids = $propriedade->getData('id');
            if ($this->tmData == 0 && count($ids) > 0) {
                $this->tmData = count($ids);
            }
            if (count($ids) > 0) {
                for ($i = 0; $i < $this->tmData; $i++) {
                    $this->data[$i][$this->xml->getColuna()] = $ids[$i % $this->tmData];
                }
                $this->set($this->xml->getAtributo(), $ids[0]);
            }
        } else if (is_array($propriedade)) {
            if ($this->tmData == 0 && count($propriedade) > 0) {
                $this->tmData = count($propriedade);
            }
            if (count($propriedade) > 0) {
                for ($i = 0; $i < $this->tmData; $i++) {
                    $this->data[$i][$this->xml->getColuna()] = $propriedade[$i % $this->tmData];
                }
                $this->set($this->xml->getAtributo(), $propriedade[0]);
            }
        }
        if ($this->xml->proximo()) {
            $this->montarData();
        }
    }
}
